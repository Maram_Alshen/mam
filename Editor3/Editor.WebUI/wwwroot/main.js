(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.columns{\r\n    margin: 0;\r\n}\r\n\r\n.column.is-12{\r\n    padding: 0px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"columns\">   \r\n    <div class=\"column is-2\">\r\n        <app-vendor-list></app-vendor-list>\r\n    </div>\r\n    <div class=\"column columns is-multiline\">\r\n        <div class=\"column is-12\">\r\n            <app-canvas></app-canvas>\r\n        </div>\r\n        <div class=\"here column is-12\">\r\n            <app-gnote></app-gnote>   \r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _canvas_canvas_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./canvas/canvas.component */ "./src/app/canvas/canvas.component.ts");
/* harmony import */ var _vendor_list_vendor_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./vendor-list/vendor-list.component */ "./src/app/vendor-list/vendor-list.component.ts");
/* harmony import */ var _gnote_gnote_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gnote/gnote.component */ "./src/app/gnote/gnote.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main.service */ "./src/app/main.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _canvas_canvas_component__WEBPACK_IMPORTED_MODULE_4__["CanvasComponent"],
                _vendor_list_vendor_list_component__WEBPACK_IMPORTED_MODULE_5__["VendorListComponent"],
                _gnote_gnote_component__WEBPACK_IMPORTED_MODULE_6__["GNoteComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"]
            ],
            providers: [_main_service__WEBPACK_IMPORTED_MODULE_8__["MainService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/canvas/canvas.component.css":
/*!*********************************************!*\
  !*** ./src/app/canvas/canvas.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#renderCanvas{\r\n     height: 100%; \r\n     width: 100%; \r\n    border: black 2px solid;\r\n    border-bottom: 1px white solid;\r\n}\r\n"

/***/ }),

/***/ "./src/app/canvas/canvas.component.html":
/*!**********************************************!*\
  !*** ./src/app/canvas/canvas.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<canvas id=\"renderCanvas\"></canvas>\r\n\r\n<div id=\"context\" class=\"context\">\r\n    <h1>Anatomic 1</h1>\r\n    <button onclick=\" prompt('edit')\">Edit</button>\r\n    <button onclick=\" prompt('replace')\">Replace</button>\r\n    <button (click)=\"removeVendor()\">Remove</button>      \r\n</div>\r\n\r\n<div id=\"context2\" class=\"context\">\r\n    <h1>No object selected</h1>   \r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/canvas/canvas.component.ts":
/*!********************************************!*\
  !*** ./src/app/canvas/canvas.component.ts ***!
  \********************************************/
/*! exports provided: CanvasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanvasComponent", function() { return CanvasComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../main.service */ "./src/app/main.service.ts");
/* harmony import */ var babylonjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babylonjs */ "./node_modules/babylonjs/babylon.js");
/* harmony import */ var babylonjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babylonjs__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CanvasComponent = /** @class */ (function () {
    function CanvasComponent(data) {
        this.data = data;
        this.highLightObjectList = [];
        this.clicked = true;
    }
    CanvasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createScene();
        this.animate();
        this.allListener(this);
        this.data.currentVendor.subscribe(function (vendor) {
            _this.import(vendor);
        });
    };
    // Babylon required data to start canvas  
    CanvasComponent.prototype.createScene = function () {
        this.canvas = document.getElementById('renderCanvas');
        this.engine = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["Engine"](this.canvas, true);
        this.scene = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["Scene"](this.engine);
        this.light = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["HemisphericLight"]('light1', new babylonjs__WEBPACK_IMPORTED_MODULE_2__["Vector3"](0, 1, 0), this.scene);
        this.camera = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["ArcRotateCamera"]("Camera", -21, 1.5, 20, babylonjs__WEBPACK_IMPORTED_MODULE_2__["Vector3"].Zero(), this.scene);
        this.canvas.style.backgroundColor = "white";
        this.camera.attachControl(this.canvas, true);
        // BABYLON.SceneLoader.ImportMesh("","assets/", "Tooth_2.babylon", this.scene, (result) => {
        //   result[0].scaling.x=60;
        //   result[0].scaling.y=60;
        //   result[0].scaling.z=60; 
        // })
    };
    CanvasComponent.prototype.allListener = function (mainMathods) {
        var _this = this;
        this.canvas.addEventListener('contextmenu', function (mouseEvent) {
            var pickResult = _this.scene.pick(_this.scene.pointerX, _this.scene.pointerY);
            var BabylonHightLightList = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["HighlightLayer"]("BabylonHightLightList", _this.scene);
            if (mouseEvent.ctrlKey) {
                if (pickResult.hit) {
                    _this.highLightObjectList.push(pickResult.pickedMesh);
                    BabylonHightLightList.addMesh(pickResult.pickedMesh, babylonjs__WEBPACK_IMPORTED_MODULE_2__["Color3"].White());
                }
            }
            else {
                if (pickResult.hit) {
                    _this.highLightObjectList.push(pickResult.pickedMesh);
                    BabylonHightLightList.addMesh(pickResult.pickedMesh, babylonjs__WEBPACK_IMPORTED_MODULE_2__["Color3"].White());
                }
            }
            var context;
            if (pickResult.hit) {
                var context_1 = document.getElementById('context');
                context_1.style.left = mouseEvent.clientX - 1 + 'px';
                context_1.style.top = mouseEvent.clientY - 1 + 'px';
                context_1.style.display = 'block';
                mainMathods.hideContext('context2');
            }
            else {
                var context_2 = document.getElementById('context2');
                context_2.style.left = mouseEvent.clientX - 1 + 'px';
                context_2.style.top = mouseEvent.clientY - 1 + 'px';
                context_2.style.display = 'block';
                mainMathods.hideContext('context');
            }
            mouseEvent.preventDefault();
            document.getElementById('context').addEventListener('mouseleave', function (evt) {
                mainMathods.hideContext();
            });
        }, false);
        this.canvas.addEventListener('click', function (mouseEvent) {
            var pickResult = _this.scene.pick(_this.scene.pointerX, _this.scene.pointerY);
            var BabylonHightLightList = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["HighlightLayer"]("BabylonHightLightList", _this.scene);
            if (mouseEvent.ctrlKey) {
                if (pickResult.hit) {
                    _this.highLightObjectList.push(pickResult.pickedMesh);
                    BabylonHightLightList.addMesh(pickResult.pickedMesh, babylonjs__WEBPACK_IMPORTED_MODULE_2__["Color3"].White());
                }
            }
            else {
                _this.highLightObjectList = [];
                _this.scene.effectLayers = [];
                BabylonHightLightList = new babylonjs__WEBPACK_IMPORTED_MODULE_2__["HighlightLayer"]("BabylonHightLightList", _this.scene);
                if (pickResult.hit) {
                    _this.highLightObjectList.push(pickResult.pickedMesh);
                    BabylonHightLightList.addMesh(pickResult.pickedMesh, babylonjs__WEBPACK_IMPORTED_MODULE_2__["Color3"].White());
                }
            }
        });
    };
    CanvasComponent.prototype.import = function (vendorFileName) {
        babylonjs__WEBPACK_IMPORTED_MODULE_2__["SceneLoader"].ImportMesh("", "https://bitbucket.org/MaramAlshen/assets/raw/master/", vendorFileName, this.scene, function (result) {
            result[0].scaling.x = 60;
            result[0].scaling.y = 60;
            result[0].scaling.z = 60;
        });
        this.addNote('Central incisor', 'SLBracket', 'Add');
    };
    CanvasComponent.prototype.hideContext = function (context) {
        switch (context) {
            case 'context':
                document.getElementById('context').style.display = 'none';
                break;
            case 'context2':
                document.getElementById('context2').style.display = 'none';
                break;
            default:
                document.getElementById('context').style.display = 'none';
                document.getElementById('context2').style.display = 'none';
                break;
        }
    };
    CanvasComponent.prototype.animate = function () {
        var _this = this;
        this.engine.runRenderLoop(function () {
            _this.scene.render();
        });
        window.addEventListener('resize', function () {
            _this.engine.resize();
        });
    };
    CanvasComponent.prototype.removeVendor = function () {
        for (var i = 0; i < this.highLightObjectList.length; i++) {
            if (this.highLightObjectList[i].parent) {
                this.highLightObjectList[i].parent.dispose();
            }
            else {
                this.highLightObjectList[i].dispose();
            }
        }
        this.hideContext('context');
    };
    CanvasComponent.prototype.addNote = function (anatomicName, vendorName, action) {
        this.data.generateNote(anatomicName, vendorName, action);
    };
    CanvasComponent.prototype.replaceFunc = function () {
        var note = prompt('replace');
        if (note !== null)
            this.addNote(note, 'Note', 'Replace');
        this.hideContext('context');
    };
    CanvasComponent.prototype.editFunc = function () {
        var note = prompt('edit');
        if (note !== null)
            this.addNote(note, 'Note', 'Edit');
        this.hideContext('context');
    };
    CanvasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-canvas',
            template: __webpack_require__(/*! ./canvas.component.html */ "./src/app/canvas/canvas.component.html"),
            styles: [__webpack_require__(/*! ./canvas.component.css */ "./src/app/canvas/canvas.component.css")]
        }),
        __metadata("design:paramtypes", [_main_service__WEBPACK_IMPORTED_MODULE_1__["MainService"]])
    ], CanvasComponent);
    return CanvasComponent;
}());



/***/ }),

/***/ "./src/app/gnote/gnote.component.css":
/*!*******************************************!*\
  !*** ./src/app/gnote/gnote.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\ntable{\r\n    width: 100%;\r\n    background-color: rgba(0,0,0,0.8);\r\n    border: 2px solid rgba(0,0,0,1);\r\n}\r\ntable thead tr th{\r\n    background-color: rgba(0,0,0,0.6);\r\n    color: white;\r\n}\r\ntable tbody > tr > th{\r\n    background-color: rgba(0,0,0,0.35);\r\n    color: white;\r\n    \r\n}\r\ntable tbody > tr > td{\r\n    background-color: rgba(0,0,0,0.35);\r\n    color: white;\r\n    \r\n}"

/***/ }),

/***/ "./src/app/gnote/gnote.component.html":
/*!********************************************!*\
  !*** ./src/app/gnote/gnote.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <table class=\"table\">\r\n    <thead>\r\n      <tr>\r\n        <th>ID</th>    \r\n        <th>Action</th>\r\n        <th>Time</th>\r\n        <th>Status</th>\r\n        <th>Description</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr>\r\n        <th>0</th>\r\n        <td>Remove</td>\r\n        <td>23:34</td>\r\n        <td style=\"color: red\">Not saved!</td>\r\n        <td>Remove vendor 2 from Anatomic 1 </td>\r\n      </tr>\r\n    </tbody>\r\n</table> -->\r\n\r\n<table class='table' *ngIf=\"allNotes\">\r\n    <thead>\r\n        <tr>\r\n          <th>ID</th> \r\n          <th>Time</th>   \r\n          <th>Action</th>\r\n          <th>Anatomic Object</th>\r\n          <th>Vendor</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr *ngFor=\"let note of allNotes\">\r\n            <td>...</td>\r\n            <td>{{ note.time }}</td>\r\n            <td>{{ note.action }}</td>\r\n            <td>{{ note.anatomicName }}</td>\r\n            <td>{{ note.vendorName }}</td>  \r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<button id=\"importVendor\" class=\"btn btn-black\" (click)=\"saveAll()\">Save</button>  \r\n"

/***/ }),

/***/ "./src/app/gnote/gnote.component.ts":
/*!******************************************!*\
  !*** ./src/app/gnote/gnote.component.ts ***!
  \******************************************/
/*! exports provided: GNoteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GNoteComponent", function() { return GNoteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../main.service */ "./src/app/main.service.ts");
/* harmony import */ var babylonjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babylonjs */ "./node_modules/babylonjs/babylon.js");
/* harmony import */ var babylonjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babylonjs__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GNoteComponent = /** @class */ (function () {
    function GNoteComponent(_demoService, data) {
        this._demoService = _demoService;
        this.data = data;
        this.allNotes = [];
    }
    GNoteComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.allNotes = this.getNotes();
        this.getNotes();
        this.data.currentObj.subscribe(function (object) {
            _this.allNotes.push(object);
        });
        console.log(this.allNotes, ' here all ');
    };
    GNoteComponent.prototype.noteInfo = function (object) { };
    GNoteComponent.prototype.getNotes = function () {
        var _this = this;
        this._demoService.getNotes().subscribe(function (data) {
            _this.notes = data;
            console.log('data ', data);
            if (_this.notes[_this.notes.length - 1]['anatomicName'] === 'Add') {
                babylonjs__WEBPACK_IMPORTED_MODULE_2__["SceneLoader"].ImportMesh("", "assets/", 'Bracket_3.babylon', _this.scene, function (result) {
                });
            }
        }, 
        // the second argument is a function which runs on error
        function (err) { return console.error(err); }, 
        // the third argument is a function which runs on completion
        function () { return console.log('done loading notes'); });
    };
    // public postAllNotes() : void {
    //   this._demoService.postAllNotes(this.allNotes).subscribe(
    //     // the first argument is a function which runs on success
    //     data => { this.notes = data},
    //     // the second argument is a function which runs on error
    //     err => console.error(err),
    //     // the third argument is a function which runs on completion
    //     () => console.log('done loading notes')
    //   );
    // }
    GNoteComponent.prototype.saveAll = function () {
        this.data.sendArray(this.allNotes);
    };
    GNoteComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gnote',
            template: __webpack_require__(/*! ./gnote.component.html */ "./src/app/gnote/gnote.component.html"),
            styles: [__webpack_require__(/*! ./gnote.component.css */ "./src/app/gnote/gnote.component.css")]
        }),
        __metadata("design:paramtypes", [_main_service__WEBPACK_IMPORTED_MODULE_1__["MainService"], _main_service__WEBPACK_IMPORTED_MODULE_1__["MainService"]])
    ], GNoteComponent);
    return GNoteComponent;
}());



/***/ }),

/***/ "./src/app/main.service.ts":
/*!*********************************!*\
  !*** ./src/app/main.service.ts ***!
  \*********************************/
/*! exports provided: MainService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainService", function() { return MainService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var MainService = /** @class */ (function () {
    function MainService(http) {
        this.http = http;
        this.vendorName = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('');
        this.currentVendor = this.vendorName.asObservable();
        this.objName = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]({});
        this.currentObj = this.objName.asObservable();
        this.arrName = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
        this.currentArr = this.arrName.asObservable();
    }
    MainService.prototype.addVendor = function (vendorFileName) {
        this.vendorName.next(vendorFileName);
    };
    MainService.prototype.generateNote = function (anatomicName, vendorName, action) {
        var obj = {
            action: action,
            vendorName: vendorName,
            anatomicName: anatomicName,
            time: Date()
        };
        this.objName.next(obj);
    };
    MainService.prototype.sendArray = function (arr) {
        this.arrName.next(arr);
        this.postAllNotes(JSON.stringify(this.arrName.value));
        return this.http.post('/api/gnote', { data: arr });
    };
    MainService.prototype.getNotes = function () {
        return this.http.get('/api/gnote');
    };
    MainService.prototype.postAllNotes = function (data) {
    };
    MainService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], MainService);
    return MainService;
}());



/***/ }),

/***/ "./src/app/vendor-list/vendor-list.component.css":
/*!*******************************************************!*\
  !*** ./src/app/vendor-list/vendor-list.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".vendorList{\r\n    border: black 2px solid;\r\n    background-color:rgba(0,0,0,0.8);\r\n    height: 100%;\r\n    width: 100%;\r\n    text-align: center;\r\n}\r\n\r\n.vendorList h1{\r\n    color: white;\r\n    text-align: center;\r\n    font-size: 30px;\r\n}\r\n\r\n.select{\r\n    width: 90%;\r\n}\r\n\r\n#selectOption{\r\n    width: 100%;\r\n    background: rgba(0,0,0,0.2);\r\n    border: 0px;\r\n    border-bottom: 1px solid white;\r\n    border-top: 1px solid white;\r\n    \r\n}\r\n\r\n.option{\r\n    text-align: center;\r\n    color: white;\r\n    font-weight: bold;\r\n    display: block;\r\n    /* background-color: #35414a; */\r\n    background-color: rgba(0,0,0,0.2);\r\n    font-size: 20px;\r\n    display: flex;\r\n    align-items: center;\r\n    border-bottom: solid 1px white;\r\n    padding-bottom: 2px;\r\n    padding-top: 2px;\r\n    \r\n}\r\n\r\n.option img{\r\n    height: 40px;\r\n}\r\n\r\n.option:hover{\r\n    background-color: rgba(100,100,255,0.5);\r\n}\r\n"

/***/ }),

/***/ "./src/app/vendor-list/vendor-list.component.html":
/*!********************************************************!*\
  !*** ./src/app/vendor-list/vendor-list.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"vendorList\">\r\n  <h1>Add Bracket</h1>\r\n  <div class=\"selectDiv\">\r\n    <select id=\"selectOption\" multiple=multiple size=\"10\">\r\n      <option class=\"option\" value=\"Central_Incisor_21n.babylon\"><img src=\"assets/ObjectNote.png\">Central_Incisor_21 1P</option>\r\n      <option class=\"option\" value=\"Central_Incisor_11n.babylon\"><img src=\"assets/ObjectNote.png\">Central_Incisor_11 1P</option>\r\n      <option class=\"option\" value=\"Central_Incisor_21.babylon\"><img src=\"assets/ObjectNote.png\">Central_Incisor_21 2P</option>\r\n      <option class=\"option\" value=\"Central_Incisor_11.babylon\"><img src=\"assets/ObjectNote.png\">Central_Incisor_11 2P</option>\r\n      <option class=\"option\" value=\"Teeth_Model_Babylon.babylon\"><img src=\"assets/ObjectNote.png\">Teeth_Model_Babylon</option>\r\n      <option class=\"option\" value=\"Canine.babylon\"><img src=\"assets/ObjectNote.png\">tooth_3_AncrePoint</option>\r\n      <option class=\"option\" value=\"Tooth_2.babylon\"><img src=\"assets/ObjectNote.png\">Tooth_2</option>\r\n      <option class=\"option\" value=\"Central_Incisor_21NEW.babylon\"><img src=\"assets/ObjectNote.png\">Central_Incisor_21NEW</option>\r\n      <option class=\"option\" value=\".babylon\"></option>\r\n      <option class=\"option\" value=\"SL_Bracket_21.babylon\"><img src=\"assets/ObjectNote.png\">SL_Bracket_21</option>\r\n      <option class=\"option\" value=\"SL_Bracket_11.babylon\"><img src=\"assets/ObjectNote.png\">SL_Bracket_11</option>\r\n      <option class=\"option\" value=\"SL_Bracket_21n.babylon\"><img src=\"assets/ObjectNote.png\">SL_Bracket_21n</option>\r\n      <option class=\"option\" value=\"SL_Bracket_21TWO.babylon\"><img src=\"assets/ObjectNote.png\">SL_Bracket_21TWO</option>\r\n      <option class=\"option\" value=\"Ammaal.babylon\"><img src=\"assets/ObjectNote.png\">Brucket_2Meshes.babylon</option>\r\n      <option class=\"option\" value=\"Bracket_3.babylon\"><img src=\"assets/ObjectNote.png\">Bracket_3</option>\r\n    </select>\r\n  </div>\r\n  <br>\r\n  <button id=\"importVendor\" class=\"btn btn-black\" (click)=\"addVendor()\">import</button>    \r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- <img src=\"assets/ObjectNote.png\"> -->"

/***/ }),

/***/ "./src/app/vendor-list/vendor-list.component.ts":
/*!******************************************************!*\
  !*** ./src/app/vendor-list/vendor-list.component.ts ***!
  \******************************************************/
/*! exports provided: VendorListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorListComponent", function() { return VendorListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../main.service */ "./src/app/main.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VendorListComponent = /** @class */ (function () {
    function VendorListComponent(data) {
        this.data = data;
    }
    VendorListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.currentVendor.subscribe(function (vendor) { return _this.vendorName = vendor; });
        // var importVendor=document.getElementById('importVendor');
        // importVendor.addEventListener('click',function () {
        //   document.getElementById('selectOption').value;
        // 
        // })
    };
    VendorListComponent.prototype.addVendor = function () {
        var vendorFileName = document.getElementById('selectOption').value;
        this.data.addVendor(vendorFileName);
    };
    VendorListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vendor-list',
            template: __webpack_require__(/*! ./vendor-list.component.html */ "./src/app/vendor-list/vendor-list.component.html"),
            styles: [__webpack_require__(/*! ./vendor-list.component.css */ "./src/app/vendor-list/vendor-list.component.css")]
        }),
        __metadata("design:paramtypes", [_main_service__WEBPACK_IMPORTED_MODULE_1__["MainService"]])
    ], VendorListComponent);
    return VendorListComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\LENOVO\Documents\mam\Editor3\Editor.WebUI\client-side\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map